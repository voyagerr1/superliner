﻿using UnityEngine;
using System.Collections;

public class destroyBullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Enemy") {
			Destroy (gameObject);
		} else {

			StartCoroutine(wait ());
		}
	}

	IEnumerator wait(){
		yield return new WaitForSeconds (2);
		Destroy (gameObject);
	}

}

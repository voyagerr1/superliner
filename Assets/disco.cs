﻿using UnityEngine;
using System.Collections;

public class disco : MonoBehaviour {

	public Camera cam;
	public float nextColorTime = 0.0f;
	public float colorRate = 1.10f;
	public Color myColor;

	public gameManager gm;





	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	//	cam.backgroundColor = myColor;

		if (nextColorTime < Time.time)
		{
			changeColor();




			nextColorTime = Time.time + colorRate;

			//Speed up the spawnrate for the next egg
			colorRate *= 0.98f;
			colorRate = Mathf.Clamp(colorRate, 0.3f, 99f);
		}
	}

	public void LoadGame(){

		Application.LoadLevel ("Main01");
	}

	public void LoadMenu(){
		

		Application.LoadLevel ("MainMenu");
	}

	void changeColor(){
		myColor = new Color (UnityEngine.Random.Range (0.1f, 0.7f), UnityEngine.Random.Range (0.1f, 0.7f), UnityEngine.Random.Range (0.1f, 0.7f));
	}

	public void QuitGame(){

		Application.Quit ();
	}

	public void ClearPlayerPrefs(){


		PlayerPrefs.DeleteAll ();
	}
}

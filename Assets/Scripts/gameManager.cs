﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class gameManager : MonoBehaviour {

	public static gameManager instance = null; 
	public GameObject player;
	public float score;
	public float Curscore;
	public Text scoreText;
	public Text stageText;
	public Text bestText;
	public Text lastText;
	public float point = 1f;
	public float now = 3f;
	public float startPoint;
	public float endPoint;
	public GameObject startObj;
	public GameObject endObj;
	public GameObject GameOverObj;
	public float playerPos;
	public float playerPosY;
	public float bestScore;
	public float lastScore;
	public playerMove pM;
	public float speed;

	public float curLocation;

	public float gameOverPos;

	public float curGameOverPos;

	public Slider distanceSlider;

	public Tonemapping tM;

	public bool isFalling = false;

	public bool expose;

	public phraseManager phrase;

	public Text epicText;
	public bool epic = false;
	public Color lerpedColor = Color.white;


	void Awake (){
		//Check if there is already an instance of SoundManager
		if (instance == null)
			//if not, set it to this.
			instance = this;
		//If instance already exists:
		else if (instance != this)
			//Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
			Destroy (gameObject);


		
		//Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
	//	DontDestroyOnLoad (gameObject);
	}

	// Use this for initialization
	void Start () {
		player = GameObject.FindWithTag ("Player");
		startObj = GameObject.FindWithTag ("START");
		endObj = GameObject.FindWithTag ("END");
		GameOverObj = GameObject.FindWithTag ("GAMEOVERPOS");
	
		expose = false;
		score = 0;
	}
	
	// Update is called once per frame
	void Update () {

		if (epic == true) {
			lerpedColor = Color.Lerp (Color.red, Color.green, Mathf.PingPong (Time.time, 1));

		}

		startPoint = startObj.transform.position.z;
		endPoint = startObj.transform.position.z;
		playerPos = player.transform.position.z;
		playerPosY = player.transform.position.y;
		gameOverPos = GameOverObj.transform.position.y;

		curLocation = playerPos - endPoint;
		curGameOverPos = playerPosY - gameOverPos;
		distanceSlider.value = curLocation;
		score = Curscore;


		if (expose == true) {
			tM.exposureAdjustment = Mathf.Round (curGameOverPos * 100f) / 100f * speed * Time.deltaTime	;

		} else {
			return;
		}



	}




	public void PauseGame(){

		Time.timeScale = 0;
		pM.paused = true;
	}


	public void UnPauseGame(){

		Time.timeScale = 1;
		pM.paused = false;
	}














}

﻿using UnityEngine;
using System.Collections;

public class flyingPlayer : MonoBehaviour {

	public float AmbientSpeed = 100.0f;
	
	public float RotationSpeed = 100.0f;

	public float turnSpeed;

	Transform trans;

	// Use this for initialization
	void Start () 
	{
		Rigidbody rigidbody = GetComponent<Rigidbody> ();
		trans = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () 
	{

		if (Input.GetKey (KeyCode.A)) {
			trans.Translate(Vector3.left * turnSpeed * Time.deltaTime);
			//	rb.AddForce(trans.forward * speed * Time.deltaTime);
			
		}
		if (Input.GetKey (KeyCode.D)) {
			trans.Translate(Vector3.right * turnSpeed * Time.deltaTime);
			//	rb.AddForce(trans.forward * speed * Time.deltaTime);
			
		}

		if (Input.GetKey (KeyCode.W)) {
			trans.Translate(Vector3.forward * turnSpeed * Time.deltaTime);
			//	rb.AddForce(trans.forward * speed * Time.deltaTime);
			
		}
		
		if (Input.GetKey (KeyCode.S)) {
			trans.Translate(Vector3.back * turnSpeed * Time.deltaTime);
			//	rb.AddForce(trans.forward * speed * Time.deltaTime);
			
		}
	}
		

	
	void FixedUpdate()
	{
		UpdateFunction();
	}
	
	void UpdateFunction()
	{
		
		Quaternion AddRot = Quaternion.identity;
		float roll = 0;
		float pitch = 0;
		float yaw = 0;
		roll = Input.GetAxis("Mouse X") * (Time.fixedDeltaTime * RotationSpeed);
		pitch = Input.GetAxis("Mouse Y") * (Time.fixedDeltaTime * RotationSpeed);
		yaw = Input.GetAxis("Yaw") * (Time.fixedDeltaTime * RotationSpeed);
		AddRot.eulerAngles = new Vector3(-pitch, yaw, -roll);
		GetComponent<Rigidbody>().rotation *= AddRot;
		Vector3 AddPos = Vector3.forward;
		AddPos = GetComponent<Rigidbody>().rotation * AddPos;
		GetComponent<Rigidbody>().velocity = AddPos * (Time.fixedDeltaTime * AmbientSpeed);
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using CnControls;
using System.Collections;
using System.Collections.Generic;
public class playerMove : MonoBehaviour {

		
	Transform trans;
	public float speed;
	public float Bspeed;
	public float moveSpeed;

	public float jumpPower;
	public float jumpPowerUp;
	public Rigidbody rb;
	bool canJump = true;
	bool startGame = false;
	Vector3 startPos = new Vector3 (0f, 20.32f,-4.38f);
	public bool isLeftPressed = false;
	public bool isRightPressed = false;
	public bool onGround = true;
	public float maxSpeed = 100f;//Replace with your max speed
	public gameManager gm;
	public Text countDown;
	public float now = 3f;
	public float boost;
	public Transform startSpot1;
	public Color myColor;
	public Camera cam;
	private bool m_isAxisInUse = false;
	private bool s_isAxisInUse = false;
	public rotateX spawnX;
	public bool pUdbl = false;
	public bool paused;
	public float CD = 3f;
	public AudioSource audio;
	public soundEffects sE;

	public bool	textBlank = false;
	public GameObject jumpPrefab;
	private GameObject spawn;
	public phraseManager phrase;
	// Use this for initialization

	void Awake (){


	}



	void Start () {
		trans = GetComponent<Transform> ();
		rb = GetComponent<Rigidbody> ();

		preLoadPanel.SetActive(false);

		StartCoroutine (starter ());


		gm.bestScore = PlayerPrefs.GetFloat ("Best Score");

		gm.bestText.text = gm.bestScore.ToString ();

	}


	
	// Update is called once per frame
	void Update () {
		


		if (now > 0 && paused == false) {
			

			gm.Curscore = Mathf.RoundToInt(gm.curLocation) * 10;
			gm.scoreText.text = gm.Curscore.ToString ();

		}


		if (paused == true) {
			gm.Curscore = gm.Curscore;
			gm.scoreText.text = gm.Curscore.ToString ();
		}


		if (pUdbl == true) {
			
			moveSpeed = 4;
			StartCoroutine(PowerStopCD (CD));

		}



		Vector3 turnRot = new Vector3 (CnInputManager.GetAxis ("Horizontal"), 0f, 0f).normalized; 



		trans.Translate (turnRot * Time.deltaTime * speed);

		

		if (CnInputManager.GetButtonDown("Jump") && onGround == true){

			jump ();
		}

		if (startGame == true) { 
			trans.Translate (Vector3.forward * Time.deltaTime * moveSpeed);

		} else {
			return;
		}

//		if (isLeftPressed) {
			//Your code Here
	//		rotateLeft ();
	//	} else if (isRightPressed) {
			
			//Your Code Here
	//		rotateRight ();
	//	} else {
	//		return;
	//	}
	//

		                      
		Debug.Log (turnRot);
		



		if (CnInputManager.GetAxisRaw("Horizontal") != 0){
			if (m_isAxisInUse == false) {
				// Call your event function here.
				changeColor ();
				m_isAxisInUse = true;
			}

		}
		if (CnInputManager.GetAxisRaw("Horizontal") == 0){

			m_isAxisInUse = false;


		}



		/*
		if (Input.GetKey (KeyCode.A)) {
			trans.Rotate(Vector2.down * Time.deltaTime * speed);
			//	rb.AddForce(trans.forward * speed * Time.deltaTime);
			
		}
		if (Input.GetKey (KeyCode.D)) {
			trans.Rotate(Vector2.up * Time.deltaTime * speed);
			//	rb.AddForce(trans.forward * speed * Time.deltaTime);
			
		}



		if (Input.GetKeyDown(KeyCode.Space)){
			
			rb.AddForce(Vector3.up * jumpPower);
			
		}
		*/

	}

	IEnumerator PowerStopCD (float cooldown){
		yield return new WaitForSeconds (cooldown);
		CD = 3;
		moveSpeed = 2.1f;
		pUdbl = false;

	}



	void FixedUpdate()
	{
		Camera.main.backgroundColor = myColor;


		if(rb.velocity.magnitude > maxSpeed)
		{
			rb.velocity = rb.velocity.normalized * maxSpeed;
		}




	}

	IEnumerator starter(){
		rb.isKinematic = true;
		gm.expose = false;
		phrase.phraseText.text = "";
		gm.tM.exposureAdjustment = 1.92f;
		yield return new WaitForSeconds (1);
		rb.isKinematic = false;
		startGame = true;

			
			
	}
	public void jump(){
		
		audio.clip = sE.jump;
		audio.Play();
		changeColor ();
			rb.AddForce (Vector3.up * jumpPowerUp);
		rb.AddForce (Vector3.forward * jumpPower);
			


	}




	IEnumerator waitJump(){
		yield return new WaitForSeconds (1);
		onGround = true;
		StopAllCoroutines ();
	}


	/*
	public void rotateRight(){

			trans.Rotate(Vector2.up * Time.deltaTime * speed);

			
			//	rb.AddForce(trans.forward * speed * Time.deltaTime);
			

	}

	public void rotateLeft(){

			trans.Rotate(Vector2.down * Time.deltaTime * speed);
			//	rb.AddForce(trans.forward * speed * Time.deltaTime);
			

	}
*/

	public GameObject preLoadPanel;

	void OnTriggerEnter(Collider other){

		float step = speed * Time.deltaTime;
		if (other.tag == "GameOver") {
			moveSpeed = 2.1f;
	//		Destroy(gameObject);
			gm.lastScore = gm.Curscore;
			pUdbl = false;
			gm.expose = false;
			gm.isFalling = false;
			if (gm.lastScore > gm.bestScore) {
				
				gm.bestScore = gm.lastScore;
				PlayerPrefs.SetFloat ("Best Score", gm.bestScore);
				gm.bestText.text = gm.lastScore.ToString ();
			} 

			spawnX.spinIt (false);
			gm.Curscore = 0;
			gm.score = 0;

			rb.isKinematic = true;
			trans.eulerAngles = new Vector3 (0, 0, 0);
			trans.position = startSpot1.position;

		//	StartCoroutine (starter ());
			preLoadPanel.SetActive(true);


			Application.LoadLevel("Main01");

		}

		if (other.tag == "FALLING") {
			gm.isFalling = true;
			gm.expose = true;
			textBlank = true;
			StartCoroutine(waitText());
		}

		if (other.tag == "Win") {
			
			//		Destroy(gameObject);
			
			Application.LoadLevel("MainMenu");
			
		}


		if (other.tag == "sGame") {
			StartCoroutine(starter());
			rb.isKinematic = false;
		}

		if (other.tag == "Coin") {
			gm.Curscore = gm.Curscore + 5;
			gm.scoreText.text = "Score: " + gm.Curscore;
		}

		if (other.tag == "spawnX") {
			spawnX.spinIt (true);



		} 

		if (other.tag == "powerUp") {
			CD = CD + 3;
			pUdbl = true;
		}

		if (other.tag == "EPIC") {
			CD = CD + 20;
			gm.epic = true;
			pUdbl = true;
		}


		if (other.tag == "slowDown" && pUdbl == true) {
			moveSpeed = -6f;
			StartCoroutine (waitSlow ());
		} 

		if (other.tag == "slowDown") {
			
			StartCoroutine (waitSlow ());
		} 

	}

	IEnumerator waitSlow(){
		moveSpeed = -6f;
		yield return new WaitForSeconds (1);
		moveSpeed = 2.1f;
	}



	void OnCollisionStay(Collision col){

		if (col.gameObject.tag == "ground") {
			onGround = true;
			if (!audio.isPlaying) {
				audio.clip = sE.grind;
				audio.loop = true;
				spawn = (GameObject)Instantiate (jumpPrefab, this.gameObject.transform.position, Quaternion.identity);
				audio.Play ();
			} else {
				audio.loop = false;
			}

		} 
	}



	void OnCollisionEnter(Collision other){
		if (other.gameObject.tag == "ground") {
			onGround = true;

				
				audio.clip = sE.land;
				audio.Play ();

		} 
		if (other.gameObject.tag == "speedUp") {

			rb.AddForce (Vector3.forward * Time.deltaTime * boost);
			
		} 





	}

	void OnCollisionExit(Collision other){
		if (other.gameObject.tag == "ground") {
			onGround = false;

		}
	}

	public void changeColor(){
		myColor = new Color (UnityEngine.Random.Range (0.2f, 1f), UnityEngine.Random.Range (0.2f, 1f), UnityEngine.Random.Range (0.09f, 1f));

	}

	public void changeColorA(){
		myColor = new Color (UnityEngine.Random.Range (0.3f, 0.9f), UnityEngine.Random.Range (0.3f, 0.8f), UnityEngine.Random.Range (0.03f, 0.9f));

	}


	IEnumerator waitText(){
		if (textBlank == true) {	
			phrase.playRandomPhrase ();
			yield return new WaitForSeconds (1);

		}
	}



	public void onPointerDownLeftButton()
	{
		isLeftPressed = true;
	}
	public void onPointerUpLeftButton()
	{
		isLeftPressed = false;
	}
	public void onPointerDownRightButton()
	{
		isRightPressed = true;
	}
	public void onPointerUpRightButton()
	{
		isRightPressed = false;
	}


}



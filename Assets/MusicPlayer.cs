﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

	private static MusicPlayer instance = null;
	public static MusicPlayer Instance{
		get { return instance;}
	}


	Object[] myMusic;
	public AudioSource audio;

	int i=0;


	void Awake (){

		if (instance != null && instance != this) {
			Destroy (this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad (this.gameObject);

	
		myMusic = Resources.LoadAll ("Music", typeof(AudioClip));



	}





	// Use this for initialization
	void Start () {

	


		audio.Play ();

	

	}
	
	// Update is called once per frame
	void Update () {
	
		if (!audio.isPlaying) {
			playRandomMusic ();
		} else {
			return;
		}


	}


	void playRandomMusic(){

		audio.clip = myMusic [Random.Range (0, myMusic.Length)] as AudioClip;
		audio.Play ();

	//	i++;
	//	if (i >= myMusic.Length)
	//		i = 0;
	//	Invoke ("playRandomMusic", audio.clip.length + 0.5f);

	}





	public void muteAudio(){
		if (audio.mute)
			audio.mute = false;
		else
			audio.mute = true;

	}




}

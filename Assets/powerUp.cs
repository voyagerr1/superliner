﻿using UnityEngine;
using System.Collections;

public class powerUp : MonoBehaviour {

	public GameObject player;
	public GameObject getPower;
	private GameObject spawn;
	private float time = 0.5f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter (Collider other){
		if (other.tag == "Player") {
			spawn = (GameObject)Instantiate (getPower, this.gameObject.transform.position, Quaternion.identity);

			Destroy (this.gameObject, time);

		} else
			return;
	}

}

﻿using UnityEngine;
using System.Collections;

public class groundChange : MonoBehaviour {

	public Color myColor;
	public Renderer ren;
	public playerMove pM;


	// Use this for initialization
	void Start () {
		ren = GetComponent<Renderer> ();

	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public	void changeColor(){
			myColor = new Color (UnityEngine.Random.Range (0.4f, 1f), UnityEngine.Random.Range (0.4f, 1f), UnityEngine.Random.Range (0.4f, 1f));
		ren.material.color = myColor;
			
	}


	void OnCollisionEnter (Collision other){

		if (other.gameObject.tag == "Player") {
			changeColor ();
			pM.changeColorA ();


		} else {
			return;
		}

	}
}
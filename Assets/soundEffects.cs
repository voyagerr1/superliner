﻿using UnityEngine;
using System.Collections;

public class soundEffects : MonoBehaviour {

	private static soundEffects instance = null;
	public static soundEffects Instance{
		get { return instance;}
	}



	public AudioClip jump;
	public AudioClip land;
	public AudioClip grind;
	public AudioSource audio;

	void Awake(){
		if (instance != null && instance != this) {
			Destroy (this.gameObject);
			return;
		} else {
			instance = this;
		}
	//	DontDestroyOnLoad (this.gameObject);



	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class rayCast : MonoBehaviour {

	private Transform cameraTransform;
	private Camera cam;

	void Start(){
		cameraTransform = GameObject.Find("Main Camera").transform;
		cam = cameraTransform.GetComponent<Camera>();
	}

	void Update(){
		if (Input.GetMouseButtonDown(0)) {
			
			Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 10000)){
				Debug.DrawLine(ray.origin, hit.point);
				Debug.Log("Clicked on " + hit.transform.gameObject.name); 
				Destroy(hit.transform.gameObject);
			}        
		}
	}

}
